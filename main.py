import pandas as pd
from flask import Flask, render_template, request, jsonify

from rama4d.features import num2text
from rama4d.pipeline import predict as predict_ciiu

app = Flask(__name__, template_folder='web')


def get_ciiu_data(ciiu_4d):
    try:
        data = ciiu_df.query(f'Clase == {ciiu_4d}')
        return data['Descripción'].values[0], data['Incluye'].values[0].replace('\n', '</br>')
    except KeyError:
        return '', ''


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/predict', methods=['POST'])
def predict():
    p6370 = request.form['p6370']
    p6390 = request.form['p6390']
    p6430 = int(request.form['p6430'])
    p6880 = int(request.form['p6880'])

    text = f'{p6370}. {p6390}. {num2text(p6430, var="P6430")}. {num2text(p6880, var="P6880")}'

    pred = predict_ciiu(text)

    # Get variables
    rama4d_code = pred["RAMA4D_R4_PRED"]
    proba = pred["PROBA"]
    descr, includes = get_ciiu_data(rama4d_code)

    pred_msg = f'<b>Código CIIU:</b> {int(rama4d_code):04d}</br>Probabilidad: {proba * 100:.1f}%'
    pred_msg += f'</br></br><b>{descr}</b></br>{includes}'
    if proba < 0.33:
        class_prop = 'alert alert-danger'
    elif 0.33 <= proba < 0.66:
        class_prop = 'alert alert-warning'
    elif proba >= 0.66:
        class_prop = 'alert alert-info'

    if text.replace('.', '').replace(' ', '') == '':
        pred_msg = '...'
        class_prop = 'alert alert-primary'

    return jsonify({
        'text': text,
        'pred': pred_msg,
        'class': class_prop
    })


if __name__ == '__main__':
    ciiu_df = pd.read_excel('ciiu/ciiu_diccionario.xlsx', skiprows=2)

    app.run(debug=False, threaded=False, host='0.0.0.0')
